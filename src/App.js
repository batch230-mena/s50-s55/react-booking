//import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
//import { Fragment } from 'react'
import {BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; // s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

import Settings from './pages/Settings';

import { UserProvider } from './UserContext';
import { useState, useEffect, useContext } from 'react';

// import Banner from './components/Banner';
// import Highlights from './components/Highlights';


function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    id: localStorage.getItem('id')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    /* React Fragments allows us to return multiple elements */
    
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      
      <Container fluid>
        <AppNavbar />  
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/courses/:courseId" element={<CourseView />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/settings" element={<Settings />} />
          <Route path="*" element={<Error />} />
        </Routes>
        
      </Container>
    </Router>
    </UserProvider>

  );
}

export default App;
