import { Card, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {

  const {_id, name, description, price} = courseProp;

  return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
          </Card.Body>
      </Card>
  )
}

// Check if the CourseCard component is getting the correct property types
/*
CourseCard.propTypes = {
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
*/